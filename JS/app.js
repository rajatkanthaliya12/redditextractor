var app = angular.module('myApp', ["ngRoute", "ui.bootstrap"]);

app.config(function($routeProvider) {
  $routeProvider
    .when("/home", {
      templateUrl: "Page/home.html",
      controller: "HomeCtrl"
    })
    .when("/comment/:url*",{
      templateUrl:"Page/comment.html",
      controller:"CommentCtrl"
    })
    
    .otherwise({
      redirectTo: "/home"
    });
});