app.controller('CommentCtrl', function($scope, dataService, $routeParams) {
  $scope.url = $routeParams.url + ".json";
  $scope.showLoading = false;
  dataService.getAll($scope.url).then(function(data) {
    $scope.content = data;
    $scope.showLoading = true;
    $scope.displayTree = $scope.content[1].data.children;
    $scope.topic = $scope.content[0].data.children[0].data;
    console.log($scope.topic);
    $scope.defaultThumbnail = "http://ts1.mm.bing.net/th?id=AdFayh8aVzMGSXw300C300&w=182&h=183&c=7&rs=1&qlt=90&pid=3.1&rm=2";
    console.log($scope.displayTree[0]);
    $scope.showReplies = function(replies) {
      if (replies.data.author) {
        return true;
      } else {
        return false
      }
    }

    setTimeout(function() {
      $(function() {
        $('.tree li:has(ul)').addClass('parent_li').find(' > span').attr('title', 'Collapse this branch');
        $('.tree li.parent_li > span').on('click', function(e) {
          var children = $(this).parent('li.parent_li').find(' > ul > li');
          if (children.is(":visible")) {
            children.hide('fast');
            $(this).attr('title', 'Expand this branch').find(' > i').addClass('icon-plus-sign').removeClass('icon-minus-sign');
          } else {
            children.show('fast');
            $(this).attr('title', 'Collapse this branch').find(' > i').addClass('icon-minus-sign').removeClass('icon-plus-sign');
          }
          e.stopPropagation();
        });
      });
    }, 0);
    console.log($scope.displayTree);
  })
});